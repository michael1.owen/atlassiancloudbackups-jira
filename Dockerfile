FROM python:3.7-alpine
WORKDIR /usr/src/app
ENV PYTHONUNBUFFERED=1
COPY requirements.txt ./
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY src/ .

EXPOSE 80

ENTRYPOINT ["python", "jirabackup.py"]