#
# --------- Information ---------
# Script amended by Michael Owen to specific purposes for setting individual variables and uploading to AWS S3, and invoking a lambda function to send a notification to Slack.
# Based on the script created by Piotr Zadrozny
# Which is based on script created by The Epic Battlebeard 10/08/18 and downloaded from https://bitbucket.org/atlassianlabs/automatic-cloud-backup/src/master/jira_backup.py
# thanks to Lukasz Grobelny for correcting the urllib3 connection headers.
# --------- Information ---------
# 

import json
import time
import boto3
import logging
import requests
import re
import urllib3
from botocore.exceptions import ClientError

print('Atlassian JIRA Python Script Started')

time.sleep(5)

# 1 Read the input parameters
print('Step 1 - Reading Input Parameters')

#****************************************
#****************************************
#Change Variables Below
#****************************************
#****************************************

#Note: Remember to place variables within single quotes.

# Atlassian account name
account = 'myatlassianaccountname'  # Find this in your atlassian URL eg: https://myatlassianaccountname.atlassian.net/

# JIRA account username (in email address format) - this is the account which generated the API Key.
username = 'dave.smith@companyname.co.uk'  

# Tells the script whether or not to pull down the attachments (true/false)
attachments = 'true'  

# Tells the script whether to export the backup for Cloud or Server
cloud = 'true'  

# Destination S3 bucket to store downloaded backup zipped file
bucket = 'myS3bucketname'  

# AWS region i.e. us-west-2, eu-west-1, eu-west-2
region = 'eu-west-2'  

# AWS Secrets Manager's Secret ARN which contains the Atlassian API Token (Note: We found it useful to have a seperate API Key for both JIRA and Confluence backups - this allowed them to execute concurrently).
secret_arn = 'arn:aws:secretsmanager:eu-west-2:123456789012:secret:Atlassian-JIRA-Backup-Token-example'  

# Name of the AWS Secret Key (not Secret Name! or Secret Value) which contains the Atlassian Token
secret_key = 'mySecretKey'  

# AWS Lambda Function Name to push notification to Slack. We use the "lambda-to-slack" application from the AWS Serverless App Repository - https://serverlessrepo.aws.amazon.com/applications/us-east-1/289559741701/lambda-to-slack)
slack_function = 'serverlessrepo-lambda-to-slack-LambdaToSlack-ABCDEFGHIJKLMNOP' 


#****************************************
#****************************************
#Change Variables Above
#****************************************
#****************************************

time.sleep(5)

# 2 Create convenient variables and open new session for cookie persistence and auth
print('Step 2 - Creating Variables')

atlassian_json_data = json.dumps({"cbAttachments": attachments, "exportToCloud": cloud})  # Atlassian backup endpoint specific parameters
url = 'https://' + account + '.atlassian.net'  # Create the full base url for the JIRA instance using the account name.

time.sleep(5)

# 3 Get Atlassian user token from AWS Secret Manager. You can create token using this instruction https://confluence.atlassian.com/cloud/api-tokens-938839638.html
print('Step 3 - Getting Atlassian User Token')

session = boto3.session.Session()
client = session.client(service_name='secretsmanager', region_name=region)
try:
    response = client.get_secret_value(SecretId=secret_arn)
except ClientError as e:
    raise e
else:
    secret = response['SecretString']
    secret_json = json.loads(secret)
    token = secret_json[secret_key]

time.sleep(5)

# 4 Open new session for cookie persistence and auth
print('Step 4 - Opening Cookie / Auth Session')

session = requests.Session()
session.auth = (username, token)
session.headers.update({"Accept": "application/json", "Content-Type": "application/json"})

time.sleep(5)

try:
    
    # 5 Start generating backup
    print('Step 5 - Requesting Backup Generation')
    backup_req = session.post(url + '/rest/backup/1/export/runbackup', data=atlassian_json_data)
    
    # 6 Catch error response from backup start and exit if error found
    
    if 'error' in backup_req.text:
            print(backup_req.text)
            exit(1)
    
    # 7 Get task ID of backup
    print('Step 7 - Getting Backup Task ID')

    task_req = session.get(url + '/rest/backup/1/export/lastTaskId')
    print('Task_Req ' + task_req.text)

    task_id = task_req.text
    print('Task_ID ' + task_id)

    # 8 set starting task progress values outside of while loop and if statements.
    print('Step 8 - Initialising Backup Progress Variables')

    task_progress = 0
    last_progress = -1
    global progress_req
    
    print('Step8End')
    print(task_id)
    
    # 9 Get progress and continue until backup generation complete
    print('Step 9 - Monitoring Backup Progress')

    while task_progress < 100:
        print('Checking Backup Generation Progress')
        progress_req = session.get(url + '/rest/backup/1/export/getProgress?taskId=' + task_id)

        # Chop just progress update from json response
        try:
            task_progress = int(re.search('(?<=progress":)(.*?)(?=,)', progress_req.text).group(1))
        except AttributeError:
            print('AttributeError')
            print('Progress_Req: ' + progress_req.text)
            exit(1)

        if (last_progress != task_progress) and 'error' not in progress_req.text:
            print('Task_Progress: ' + str(task_progress))
            last_progress = task_progress
        elif 'error' in progress_req.text:
            print('Progress_Req: ' + progress_req.text)
            exit(1)

        if task_progress < 100:
            print('Backup Generation Task Progress' + str(task_progress) + ' percent')
            time.sleep(60)


    # 10 Download generated backup zip file and stream it to s3 bucket
    if task_progress == 100:
        
        print('Step 10 - Uploading Backup')

        download = re.search('(?<=result":")(.*?)(?=\",)', progress_req.text).group(1)
        downloadurl=url + '/plugins/servlet/' + download
        print("DownloadURL: " + downloadurl)
        date = time.strftime("%Y%m%d_%H%M%S")
        print("Date: " + date)
        key = 'Atlassian/JIRA/' + account + '_backup_' + date + '.zip'
        print('Key: ' + key)
        s3=boto3.client('s3')
        http=urllib3.PoolManager()
        headers = urllib3.make_headers(basic_auth=username + ":" + token)
        s3.upload_fileobj(http.request('GET', downloadurl, preload_content=False, headers=headers), bucket, key)

        print('Complete - Sending Success Message')
        #Send Success Message 
        lambda_client = boto3.client('lambda')
        lambda_payload = json.dumps(["The JIRA backup to AWS S3 has been successfully completed."])
        lambda_client.invoke(FunctionName=slack_function, 
                     InvocationType='Event',
                     Payload=lambda_payload)
        #ProcessEnd
        print('***Process Ended - Backup Complete***')

except Exception as e:
    # 11 Creating notification in case of failure
    print('Exception Raised')

    lambda_client = boto3.client('lambda')
    lambda_payload = json.dumps(["The JIRA backup to AWS S3 has failed - please see the CloudWatch Logs for error tracing."])
    lambda_client.invoke(FunctionName=slack_function, 
                     InvocationType='Event',
                     Payload=lambda_payload)
    raise e
