# jira-cloud-backup

## Name
JIRA Cloud Backup

## Description
Provides the functionality to automatically backup your Atlassian JIRA cloud account and saves the zip output to Amazon S3 bucket.

A Python script with accompanying Dockerfile  allows an image to be built and then subsesquently pushed and stored to AWS ECR (Elastic Container Registry). 

This can then be executed via a cron job using AWS EventBridge. On success or failure, the script invokes the Slack Messaging lambda (passes a JSON Array string) to provide a live notification to Slack. 

## AWS Services Used

AWS Secrets Manager - For storing the JIRA API Token  
AWS S3 - For storing the output backup .zip  
AWS Lambda - Invoking the Slack Messaging function  
AWS ECS/ECR - For storing (ECR) amd running (ECS/Fargate) the Docker image  
AWS EventBridge - For scheduling the cron job

## Building and Pushing the Docker to AWS ECR 

Make sure that you have the latest version of the AWS CLI and Docker installed.

Instructions can be found in the AWS Console, ECR pages which gives you step by step commands to authenticate and push an image to your repository.